import axios from "axios";

const instance = axios.create({
  baseURL: "https://myburgerapp-6b4d0.firebaseio.com/"
});

export default instance;
