import React from "react";
import classes from "./MenuOpenButton.css";

const menuToggleButton = props => (
  <div className={classes.MenuOpenBtn} onClick={props.clicked}>
    <div />
    <div />
    <div />
  </div>
);

export default menuToggleButton;
