import React from "react";
import LogoText from "../../Logo/LogoText";
import NavItems from "../NavigationItems/NavigationItems";
import MenuOpenButton from "../SideDrawer/MenuOpenButton/MenuOpenButton";
import classes from "./Toolbar.css";

const toolbar = props => (
  <header className={classes.Toolbar}>
    <MenuOpenButton clicked={props.open} />
    <div className={classes.Logo}>
      <LogoText />
    </div>
    <nav className={classes.DesktopOnly}>
      <NavItems />
    </nav>
  </header>
);

export default toolbar;
