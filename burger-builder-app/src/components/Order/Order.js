import React from "react";
import classes from "./Order.css";

const order = props => {
  const ingredients = [];
  for (let ingredientName in props.ingredients) {
    ingredients.push({
      name: ingredientName,
      amount: props.ingredients[ingredientName]
    });
  }
  const ingredientsOutput = ingredients.map(ing => {
    return (
      <span
        key={ing.name}
        style={{
          textTransform: "capitalize",
          display: "inline-block",
          margin: "0 5px",
          boxShadow: "0 1px 2px #aaa",
          padding: "3px",
          color: "#555"
        }}
      >
        {ing.name}({ing.amount})
      </span>
    );
  });
  return (
    <div className={classes.Order}>
      <p>Ingredients:{ingredientsOutput}</p>
      <p>
        Price <strong>$ {props.price}</strong>
      </p>
    </div>
  );
};

export default order;
