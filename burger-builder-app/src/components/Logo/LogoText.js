import React from "react";
import classes from "./Logo.css";

const logoText = props => (
  <div className={classes.LogoText}>
    <em>
      <span>M</span>y<span>B</span>urger<span>B</span>uilder
    </em>
  </div>
);

export default logoText;
