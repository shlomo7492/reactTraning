import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../../../store/actions";

import axios from "../../../axios-orders";
import Button from "../../../components/UI/Button/Button";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Input from "../../../components/UI/Input/Input";
import classes from "./ContactData.css";

class ContactData extends Component {
  state = {
    orderForm: {
      name: {
        elementType: "input",
        elementConfig: { type: "text", name: "name", placeholder: "Your Name" },
        value: "",
        elementLabel: "Full Name:",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      street: {
        elementType: "input",
        elementConfig: {
          type: "text",
          name: "street",
          placeholder: "Your Street Address"
        },
        value: "",
        elementLabel: "Street Address:",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      postalCode: {
        elementType: "input",
        elementConfig: {
          type: "text",
          name: "postal",
          placeholder: "Your Postal Code"
        },
        value: "",
        elementLabel: "Postal Code:",
        validation: {
          required: true,
          length: 4
        },
        valid: false,
        touched: false
      },
      email: {
        elementType: "input",
        elementConfig: {
          type: "email",
          name: "email",
          placeholder: "Your Email"
        },
        value: "",
        elementLabel: "Email:",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      deliveryMethod: {
        elementType: "select",
        elementConfig: {
          options: [
            { value: "fastest", displayValue: "Fastest" },
            { value: "cheap", displayValue: "Cheap" }
          ]
        },
        value: "",
        elementLabel: "Delivery Type:",
        validation: {},
        valid: true
      }
    },
    formIsValid: false,
    loading: false
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.length) {
      isValid = value.length === rules.length && isValid;
    }
    return isValid;
  }

  orderHandler = event => {
    event.preventDefault();
    this.setState({ loading: true });
    const formData = {};
    for (let inputIdentifier in this.state.orderForm) {
      formData[inputIdentifier] = this.state.orderForm[inputIdentifier].value;
    }
    const price = (+this.props.totalPrice).toFixed(2);
    const order = {
      ingredients: this.props.ings,
      price: price,
      orderData: formData
    };
    axios
      .post("orders.json", order)
      .then(response => {
        this.props.onOrderComplete();
        this.setState({
          loading: false
        });
        this.props.history.push("/");
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };
  inputChangedHandler = (event, inputIdentifier) => {
    //event.preventDefault();

    const formUpdated = { ...this.state.orderForm };
    const updatedElement = { ...formUpdated[inputIdentifier] };
    updatedElement.value = event.target.value;
    updatedElement.valid = this.checkValidity(
      updatedElement.value,
      updatedElement.validation
    );
    updatedElement.touched = true;
    formUpdated[inputIdentifier] = updatedElement;
    console.log(updatedElement);

    let formIsValid = true;
    for (let inputIdentifier in formUpdated) {
      formIsValid = formUpdated[inputIdentifier].valid && formIsValid;
    }
    this.setState({ orderForm: formUpdated, formIsValid: formIsValid });
  };
  render() {
    const formsElements = [];
    for (let key in this.state.orderForm) {
      formsElements.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }

    let form = (
      <form className={classes.Form} onSubmit={this.orderHandler}>
        {formsElements.map(formElement => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            label={formElement.config.elementLabel}
            invalid={!formElement.config.valid}
            touched={formElement.config.touched}
            changed={event => this.inputChangedHandler(event, formElement.id)}
          />
        ))}
        <Button btnType="Success" disabled={!this.state.formIsValid}>
          ORDER
        </Button>
      </form>
    );
    if (this.state.loading) {
      form = <Spinner />;
    }
    return (
      <div className={classes.ContactData}>
        <h3>Enter your contact data!</h3>
        {form}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { ings: state.ingredients, totalPrice: state.totalPrice };
};
const mapDispatchToProps = dispatch => {
  return {
    onOrderComplete: () => dispatch({ type: actionTypes.CLEAR_BURGER })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactData);
