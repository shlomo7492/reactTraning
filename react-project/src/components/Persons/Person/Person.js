import React, { Component } from "react";
import Aux from "../../../hoc/Auxie";
import PropTypes from "prop-types";
import withClass from "../../../hoc/withClasses";
import { AuthContext } from "../../../containters/App";

import importedClasses from "./Person.css";

class Person extends Component {
  constructor(props) {
    super(props);
    console.log(
      "[Person.js] constructor is where this message comes from...",
      props
    );
    this.inputElement = React.createRef();
  }

  componentWillMount() {
    console.log(
      "[Person.js] componentWillMount() is where this message comes from..."
    );
  }

  componentDidMount() {
    console.log(
      "[Person.js] componentDidMount() is where this message comes from..."
    );
    if (this.props.position === 0) {
      this.inputElement.current.focus();
    }
    //this.focusInput();
  }

  focus() {
    this.inputElement.current.focus();
  }

  render() {
    console.log("[Person.js] render() is where this message comes from...");
    return (
      <Aux>
        <AuthContext.Consumer>
          {auth =>  auth ? <p>I am authenticated</p> : null}
        </AuthContext.Consumer>
        <p onClick={this.props.click}>
          I am {this.props.name} and I am {this.props.age} years old.
        </p>
        <input
          ref={this.inputElement}
          type="text"
          onChange={this.props.changed}
          value={this.props.name}
        />
      </Aux>
    );
  }
}

Person.PropTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
};

export default withClass(Person, importedClasses.Person);
