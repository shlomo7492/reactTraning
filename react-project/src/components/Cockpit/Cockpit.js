import React from "react";
import Aux from "../../hoc/Auxie";

import importedClasses from "./Cockpit.css";

const cockpit = props => {
  const classes = []; // ['red', 'bold'].join(' ');
  let btnClass = importedClasses.Button;

  if (props.showPersons) {
    btnClass = [importedClasses.Button, importedClasses.Red].join(" ");
  }

  if (props.persons.length <= 2) {
    classes.push(importedClasses.red);
  }
  if (props.persons.length <= 1) {
    classes.push(importedClasses.bold);
  }

  return (
    <Aux>
      <h1>{props.appTitle}</h1>
      <p className={classes.join(" ")}>This is working!!!</p>
      <button className={btnClass} onClick={props.click}>
        Togle Persons
      </button>
      <button onClick={props.login}>Log in</button>
    </Aux>
  );
};

export default cockpit;
